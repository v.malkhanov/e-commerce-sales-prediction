# E-commerce sales prediction

NSU BDA. 2021. E-commerce sales prediction.
Predict sales on the e-commerce marketplace.

# Competition description
This is a competition for NSU students. The main task is to predict number of sales of different goods on the e-commerce marketplace. You can use any ML algorithm or any ML method that you know to solve the task. You also can ask your teachers for help.

# Motivation
- All students must upload at least one submission to be admitted to the exam
- Students with TOP-10% submissions will get 5 points automatically
- Students with TOP-30% submissions will get +1 point on the exam

# Final check of the competition
- All submissions must be uploaded before 01.01.2022
- The final notebook must be uploaded to private GitLab repository before 01.01.2022
- The repository URL must be sent to your teacher through Telegram before 01.01.2022. Don't forget to give the permissions on repository to your teacher
- We will check your notebooks for exploiting data leaks or cheating. Any student who is caught with cheating or exploiting data leaks will leave the competition
- The student who pretends on extra points will have to show and run their final notebook at the pre-exam counseling. Output .csv file will be also scored there to avoid cheating

# Submission format
You should upload your submissions as .csv file wit two columns.

- ```Id``` - id of good from test.csv
- ```Expected``` - number of sales your model predicted

# Additional Info
- Somebody from the students has to make a Google Sheet with links between the Kaggle nicknames, the real names and Telegram nicknames of the students
- Your final notebook must be placed to ```{YOUR-REPO-NAME}/solution/final.ipynb```
- Data must be read from ```{YOUR-REPO-NAME}/data/test.csv```
- Submission must be placed to ```{YOUR-REPO-NAME}/submissions/submission.csv```
- Don't upload data files to your repository!
